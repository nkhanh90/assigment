# assigment

Use case of cache

1. If there is no cache in local storage, fetch all data with ticker symbol 
  ex: https://www.quandl.com/api/v3/datasets/WIKI/FB.json?column_index=4&order=asc

2. If there is cache, checking if this cache can be used
   - Image close price will be updated at 5:00pm every day. Using cache if: 

   A. if cache time > 5:00pm
   B. cache time no more than 24 hours ago and request data is earlier than 5:00pm.

3. If invalid cache, fetch data with start date = last cache time data 
  - then merge new data with current cache
  - then update cache time
  
  ex: https://www.quandl.com/api/v3/datasets/WIKI/FB.json?column_index=4&order=asc&start_date=2019-03-27



  