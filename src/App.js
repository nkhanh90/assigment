
import React, { useEffect } from 'react';

import TickerSymbolForm from './components/TickerSymbolForm';
import ClosePriceFilterForm from './components/ClosePriceFilterForm';
import ClosePriceList from './components/ClosePriceList';
import ChartClosePrice from './components/ChartClosePrice';

import useDataApiState from './hooks/useDataApiState';

import useClosePriceListState from './hooks/useClosePriceListState';
import useClosePriceFilterState from './hooks/useClosePriceFilterState';
import useChartConfigState from './hooks/useChartConfigState';
import useChartState from './hooks/useChartState';

import styled from "styled-components";

import './App.css'

function App() {

  // state manage data from fetch Api
  const { data, isLoading, isError, doFetch } = useDataApiState();
  // state manage config in highchart 
  const { chartConfig, updateSeriesData } = useChartConfigState(data);
  // render chart
  const { initChart } = useChartState();

  const { closePriceFilter, onChangeClosePriceFilter } = useClosePriceFilterState();

  const { closePriceList, updateClosePriceList, loadMore, resetVisibleItem, resetList} = useClosePriceListState();

  useEffect(() => {
    updateSeriesData(data);

    if (chartConfig.series[0].data.length > 0 ) {
      initChart(chartConfig);
    }

    onChangeClosePriceFilter('')
  }, [data]);


  useEffect(() => {
    if (closePriceFilter === '') {
      resetList();
    }
    else {
      updateClosePriceList(data.data, closePriceFilter)
    }
  }, [closePriceFilter])

  return (
    <div className="row flex-xl-nowrap">
      <Sidebar className="col-12 col-md-4 col-xl-3 py-md-5 px-md-5">
        { data && data.data.length > 0 &&
          <div>
            <ClosePriceFilterForm 
                closePriceFilter={closePriceFilter} 
                onChangeClosePriceFilter={onChangeClosePriceFilter} 
                resetVisibleItem={resetVisibleItem}
                />
            <ClosePriceList closePriceList={closePriceList} loadMore={loadMore}/>
          </div> 
        }
      </Sidebar>
      <Main className="col-12 col-md-8 col-xl-9 py-md-5 px-md-5">
        <TickerSymbolForm doFetch={doFetch}/>
        { isError && isError.response && 
          <div className="alert alert-danger" role="alert">{ isError.response.data.quandl_error.message }</div>
        }

        { isError && !isError.response &&
          <div className="alert alert-danger">Something wrong ...</div>
        }
        
        {isLoading ? ( 
          <Spinner>
            <div className="spinner-border text-primary" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          </Spinner>
        ) : (
          <ChartClosePrice/>
        )}
      </Main>
    </div>
  );
}

const Sidebar = styled.div`
  order: 1
`;

const Main = styled.div`
  margin: 0 auto;
`;

const Spinner = styled.div `
  top: 100%;
  left: 50%;
  position: absolute;
`

export default App;