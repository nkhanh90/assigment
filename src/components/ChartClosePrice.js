import React from "react";

const HighchartStock = () => {

  return (
    <div className="py-md-5 m-auto">
      <div id="result-chart" className="myChart"></div>
    </div>
  );
}

export default HighchartStock;
