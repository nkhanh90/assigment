import React from "react";

const ClosePriceFilterForm = ({ closePriceFilter, onChangeClosePriceFilter , resetVisibleItem}) => {

  return (
    <form className="form-inline" onSubmit={event => { event.preventDefault();}}>
      <div className="form-group mr-sm-3 mb-2">
        <input type="number" min="0" step="1" className="form-control" placeholder="Filter Close Price" value={closePriceFilter} 
          onChange={event => {onChangeClosePriceFilter(event.target.value); resetVisibleItem() }}
        />
      </div>
      <button type="submit" className="btn btn-primary mb-2">Find</button>
    </form>
  );
};

export default ClosePriceFilterForm;
