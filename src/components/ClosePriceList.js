import React, { Fragment} from "react";

const ClosePriceList = ({closePriceList, loadMore}) => {
  return (
    <div className="py-md-5 m-auto">
    { closePriceList.list.length > 0 &&
      <Fragment>
        <div className="text-muted">
          {closePriceList.list.length} results
        </div>
        <table className="table table-striped">
          <tbody>
            { closePriceList.list.slice(0, closePriceList.visibleItem).map((closePrice, index) => (
              <tr key={index}>
                <td>{ closePrice[0]}</td>
                <td>{closePrice[1]}</td>
              </tr>
            ))}
            { closePriceList.visibleItem < closePriceList.list.length &&
              <tr>
                <td>
                  <button className="btn btn-link" 
                    onClick={(event) => { event.preventDefault(); loadMore(closePriceList.visibleItem + 10)}}>
                    Load more
                  </button>
                </td>
              </tr>
            }
          </tbody>
        </table>
      </Fragment>
    } 
    </div>
  );
};

export default ClosePriceList;
