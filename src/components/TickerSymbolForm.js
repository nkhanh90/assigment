import React from 'react';
import useTickerSymbolInputState from './../hooks/useTickerSymbolInputState';

const TickerSymbolForm = ({doFetch}) => {
  const { tickerSymbol, onChange } = useTickerSymbolInputState();

  return (
    <form className="form-inline" onSubmit={event => { event.preventDefault(); doFetch(tickerSymbol); }}>
      <div className="form-group mr-sm-3 mb-2">
        <input className="form-control" type="text" value={tickerSymbol} 
            onChange={event => onChange(event.target.value)} placeholder="Enter ticker symbol" />
      </div>
      
      <button type="submit" className="btn btn-primary mb-2">Search</button>
    </form>
  );
};


export default TickerSymbolForm;
