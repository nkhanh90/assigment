import { useState } from 'react';

export default (initData) => {
  const [chartConfig, setChartConfig] = useState({
    clickAll: 0,
    rangeSelector: {
      selected: 4,
      buttons: [{
        type: 'month',
        count: 1,
        text: '1m'
      }, {
        type: 'month',
        count: 3,
        text: '3m'
      }, {
        type: 'month',
        count: 6,
        text: '6m'
      }, {
        type: 'month',
        count: 9,
        text: '9m'
      }, {
        type: 'year',
        count: 1,
        text: '1y'
      }, {
        type: 'year',
        count: 5,
        text: '5y'
      }, {
        type: 'all',
        text: 'All',
      }]
    },
    title: {
      text: ''
    },
    series: [{
      name: '',
      data: initData,
      tooltip: {
        valueDecimals: 2
      }
    }],
    responsive: {
      rules: [{
        condition: {
          maxWidth: 400
        },
        chartOptions: {
          legend: {
              align: 'center',
              verticalAlign: 'bottom',
              layout: 'horizontal'
          },
          yAxis: {
            labels: {
                align: 'left',
                x: 0,
                y: -5
            },
            title: {
                text: null
            }
          },
          subtitle: {
            text: null
          },
          credits: {
            enabled: false
          }
        }
      }]
    }
  });

  return {
    chartConfig,
    updateSeriesData: data => {
      const hiJson = data.data.map(function(d) {
        return [new Date(d[0]).getTime(), d[1]]
      });
      const newChartConfig = Object.assign(chartConfig, {
        title: {
          text: data.name
        },
        series: [{
          data: hiJson,
          name: data.dataset_code,
        }]
      });
      setChartConfig(newChartConfig);
    }
  };
};