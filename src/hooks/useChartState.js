import { useState } from 'react';
import * as Highcharts from "highcharts/highstock";

const useChartState = () => {
  const [chart, setChart] = useState({
    init: 0
  });

  return { 
    chart,
    initChart: config => {
      setChart(new Highcharts.stockChart('result-chart', config));
    }
  };
};

export default useChartState;