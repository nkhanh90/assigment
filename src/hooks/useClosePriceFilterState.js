import { useState } from "react";

export default () => {
  const [closePriceFilter, setClosePriceFilter] = useState('');

  return {
    closePriceFilter,
    onChangeClosePriceFilter: newClosePriceFilter => {
      setClosePriceFilter(newClosePriceFilter)
    },
  };
};


