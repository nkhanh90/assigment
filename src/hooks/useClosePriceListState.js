import { useState } from "react";

export default initialValue => {
  const [closePriceList, setClosePriceList] = useState({
    list: [],
    visibleItem: 10
  });

  return {
    closePriceList,
    resetList: () => {
      const newList = {
        visibleItem: 15,
        list: []
      }
      setClosePriceList(newList)
    },
    updateClosePriceList: (data, filter) => {
      const newClosePriceList = {
        ...closePriceList,
        list: data.filter(item => item[1] > filter)
      };
      setClosePriceList(newClosePriceList)
    },
    resetVisibleItem: () => {
      const newClosePriceList = {
        ...closePriceList,
        visibleItem: 15
      }
      setClosePriceList(newClosePriceList)
    },
    loadMore: () => {
      const newClosePriceList = {
        ...closePriceList,
        visibleItem: closePriceList.visibleItem + 20
      }
      setClosePriceList(newClosePriceList)
    }
  };
};


