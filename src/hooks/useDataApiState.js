import { useState, useEffect, useReducer } from 'react';
import axios from 'axios';
import dataFetchReducer from './useReducer.js';

const useDataApiState = () => {
  const [url, setUrl] = useState(``);
  const [code, setCode] = useState(``);

  const [state, dispatch] = useReducer(dataFetchReducer, {
    isLoading: false,
    isError: false,
    isLoaded: false,
    data: {
      data: []
    },
  });

  const getUrlApi = (tickerSymbol, dateRange = {}) => {
    const fetchTickerSymbolQuery = `https://www.quandl.com/api/v3/datasets/WIKI/${tickerSymbol}.json?column_index=4&order=asc`;
    if (dateRange.start) {
      return fetchTickerSymbolQuery + `&start_date=${dateRange.start}`;
    }
    return fetchTickerSymbolQuery;
  }

  useEffect(() => {
    let didCancel = false;

    const fetchData = async () => {
      dispatch({ type: 'FETCH_INIT' });
      try {
        // Fetch data using caches
        let cacheKey = code;
        var cached = await JSON.parse(localStorage.getItem(cacheKey));
        let whenCached = await localStorage.getItem(cacheKey + ':ts')

        // IDEA: close price is usually updated at specific time (in this case, I set 5:00pm)
        // push the new close price at today at 5pm every day
        if (cached !== null && whenCached !== null) {
          // Get timestamp of 5:10pm today
          const today = new Date();
          const todayTimestamp = today.getTime();
          var timeUpdateClosePrice = Date.parse(convertDate(today) + 'T20:50:00');

          let age = Math.round((todayTimestamp - whenCached)/(60 * 1000 * 60 * 24));
          // Valid cache: not yet update close price (todayTimestamp < timeUpdateClosePrice) or already update after close price time 
          // Using cache
          if ((age === 0 && todayTimestamp< whenCached) || (timeUpdateClosePrice < whenCached)) {
            dispatch({ type: 'FETCH_SUCCESS', payload: cached });
            return;
          } 
        }
        
        // If invalid cache, using data from fetch Api
        const result = await axios(url);
        if (!didCancel) {
          // If new data, push it in current data in cache
          if (result.data.dataset.data.length > 0) {
            const newResult = saveToLocalStorage(cacheKey, result.data.dataset, url);
            dispatch({ type: 'FETCH_SUCCESS', payload: newResult });
          }
          // else using cache
          else {
            localStorage.setItem(cacheKey + ':ts', Date.now());
            dispatch({ type: 'FETCH_SUCCESS', payload: cached });
          }
        }
      } catch (error) {
        console.log(error)
        if (!didCancel) {
          dispatch({ type: 'FETCH_FAILURE', payload: error });
        }
      }
    };

    if (!url) {
      return
    }
    else {
      fetchData();
    }
  
    return () => {
      didCancel = true;
    };
  }, [url]);

  const saveToLocalStorage = (cacheKey, data, url) => {
    let currentCached = JSON.parse(localStorage.getItem(cacheKey));
    let newCached = currentCached;
    if (url.includes('start_date')) {
      data.data.forEach((item) => {
        const newItem = [item[0], item[1]];
        newCached.data.push(newItem);
      })
      newCached.end_date = data.end_date;
    }
    else {
      newCached = data
    }
  
    localStorage.setItem(cacheKey, JSON.stringify(newCached));
    localStorage.setItem(cacheKey + ":count", data.data.length);
    localStorage.setItem(cacheKey + ':ts', Date.now());
    return newCached;
  }

  // Convert date to format ('y-m-d')
  const convertDate = (date) => {
    var dd = date.getDate();
    var mm = date.getMonth()+1; 
    var yyyy = date.getFullYear();
    if(dd < 10)  dd='0'+dd;
    if(mm < 10) mm='0'+mm;
    date = yyyy + '-' + mm + '-' + dd;
    return date;
  }

  const doFetch = (tickerSymbol) => {
    setCode(tickerSymbol)

    var cached = JSON.parse(localStorage.getItem(tickerSymbol));
    let whenCached = localStorage.getItem(tickerSymbol + ':ts')
    let newUrl;
    // If no cached, I query all data
    // If cached, I only query data that start date whenCached
    if (!cached && !whenCached) {
      newUrl = getUrlApi(tickerSymbol);
    }
    else {
      const startDate = convertDate(new Date(parseInt(whenCached, 10)));
      const dateRange = {
        start: startDate,
      }
      newUrl = getUrlApi(tickerSymbol, dateRange)
    }
    setUrl(newUrl);
  };

  return { ...state, doFetch };
};

export default useDataApiState;
