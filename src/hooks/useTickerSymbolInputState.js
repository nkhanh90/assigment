import { useState } from 'react';

export default () => {
  const [tickerSymbol, setTickerSymbol] = useState('');

  return {
    tickerSymbol,
    onChange: tS => {
      setTickerSymbol(tS);
    },
    reset: () => setTickerSymbol('')
  };
};